import React from "react";

function Hero() {
  return (
    <div class="flex align-middle justify-center ">
      <img src="./illustration/logo.svg" alt="ribbon yellow" class="w-50" />
    </div>
  );
}

export default Hero;
