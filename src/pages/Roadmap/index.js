import React from "react";
import bgTest from "../../assets/image/bgtest.png";
import flokidesRun from "../../assets/image/MetaFlokiRush.png";
import phase1 from "../../assets/image/phase1.svg";
import phase2 from "../../assets/image/phase2.svg";
import phase3 from "../../assets/image/phase3.svg";
import phase4 from "../../assets/image/phase4.svg";
import roadmapRibbon from "../../assets/image/roadmapRibbon.png";

const Roadmap = () => {
  return (
    <div>
      <div
        style={{
          backgroundImage: `url(${bgTest}) `,
          backgroundRepeat: "no-repeat",
          height: 850,
          // width: 5000,
          // marginLeft: 150,
          marginTop: -50,
        }}
      >
        <div style={{ width: "100%", marginLeft: 600 }}>
          <img
            src={roadmapRibbon}
            alt="roadmapRibbon"
            style={{ width: "40%", paddingTop: 60 }}
          />
        </div>
        <div class="flex ml-64 mt-16">
          <div>
            <div class="flex">
              <div class="animate-fade-in-down">
                <img src={phase1} alt="phase1" />
              </div>
              <div class="animate-fade-in-down">
                <img src={phase2} alt="phase2" />
              </div>
            </div>
            <div class="flex">
              <div class="animate-fade-in-up">
                <img src={phase3} alt="phase3" />
              </div>
              <div class="animate-fade-in-up">
                <img src={phase4} alt="phase4" />
              </div>
            </div>
          </div>
          <div style={{ marginTop: -70 }}>
            <img src={flokidesRun} alt="roadmapDog" style={{ width: 550 }} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Roadmap;
