import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import HowToBuy from "./HowToBuy";
import Info from "./Info";
import Roadmap from "./Roadmap";
import Tokenomics from "./Tokenomics";
function App() {
  return (
    <div>
      <Header />
      <Info />
      <Roadmap />
      <Tokenomics />
      <HowToBuy />
      <Footer />
    </div>
  );
}

export default App;
