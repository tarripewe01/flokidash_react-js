import React from "react";
import bgToken from "../../assets/image/bgToken.svg";
import tokenIcon from "../../assets/image/tokenIcon.svg";

const Tokenomics = () => {
  return (
    <div
      style={{
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
      }}
    >
      <div
        style={{
          backgroundImage: `url(${bgToken}) `,
          backgroundRepeat: "no-repeat",
          height: 920,
          marginTop: -40,
          marginLeft: -30,
        }}
      >
        <div class=" translate-y-96 translate-x-96" style={{ marginLeft: 80 }}>
          <img src={tokenIcon} alt="tokenIcon" />
        </div>
        <div
          class="flex justify-center gap-1 ml-20"
          style={{ marginTop: 350, fontFamily: "Arvo" }}
        >
          <div style={{ textAlign: "center", width: 250 }}>
            <p style={{ color: "#3AB682", fontSize: 30, fontWeight: "bold" }}>
              Total Supply
            </p>
            <p style={{ fontSize: 27, fontWeight: "500", opacity: 0.7 }}>
              1,000,000,000,000
            </p>
          </div>
          <div style={{ textAlign: "center", width: 250 }}>
            <p style={{ color: "#F2A458", fontSize: 30, fontWeight: "bold" }}>
              Initial Burn
            </p>
            <p
              style={{
                fontSize: 27,
                fontWeight: "500",
                opacity: 0.7,
                marginBottom: 80,
              }}
            >
              20% burn on presale
            </p>
            <p style={{ fontSize: 27, fontWeight: "500", opacity: 0.7 }}>
              13% burn on future events
            </p>
          </div>
          <div style={{ textAlign: "center", width: 250 }}>
            <p style={{ color: "#E8646D", fontSize: 30, fontWeight: "bold" }}>
              Rewards
            </p>
            <p style={{ fontSize: 27, fontWeight: "500", opacity: 0.7 }}>
              4 - 6% of transaction allocated for <br />
              ADA Rewards
            </p>
          </div>
          <div style={{ textAlign: "center", width: 250 }}>
            <p
              style={{
                color: "#2D9AEF",
                fontSize: 30,
                fontWeight: "bold",
                marginLeft: -30,
              }}
            >
              Others
            </p>
            <p
              style={{
                fontSize: 27,
                fontWeight: "500",
                opacity: 0.7,
                marginBottom: 120,
              }}
            >
              2 - 5% Marketing
            </p>
            <p style={{ fontSize: 27, fontWeight: "500", opacity: 0.7 }}>
              1.5% Liquidity Pool
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tokenomics;
