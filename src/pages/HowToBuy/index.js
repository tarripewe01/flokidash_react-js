import React from "react";
import bgBuy from "../../assets/image/bgBuy.svg";
import buyRibbon from "../../assets/image/buyRibbon.svg";
import coin from "../../assets/image/coin.svg";
import flokirush from "../../assets/image/flokirush.svg";
import flush from "../../assets/image/flush.svg";
import pancake from "../../assets/image/pancake.svg";
import pita1 from "../../assets/image/pita1.svg";
import pita2 from "../../assets/image/pita2.svg";
import pita3 from "../../assets/image/pita3.svg";
import pita4 from "../../assets/image/pita4.svg";
import pita5 from "../../assets/image/pita5.svg";
import wallet from "../../assets/image/wallet.svg";

const HowToBuy = () => {
  return (
    <div
      style={{
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
      }}
    >
      <div
        style={{
          backgroundImage: `url(${bgBuy}) `,
          backgroundRepeat: "no-repeat",
          height: 1200,
          width: "100%",
        }}
      >
        <div style={{ backgrounColor: "red", width: "100%", marginLeft: 550 }}>
          <img src={buyRibbon} alt="buyRibbon" style={{ width: "40%" }} />
        </div>
        <div
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginTop: -40,
            fontFamily: "Arvo",
          }}
        >
          <div class="flex gap-10 translate-x-96">
            <div
              style={{
                height: 330,
                width: 330,
                backgroundColor: "#EBEBEB",
                borderRadius: 10,
                borderWidth: 10,
                borderColor: "#FFF568",
              }}
            >
              <img
                src={wallet}
                alt="wallet"
                style={{ marginLeft: 75, marginTop: 10 }}
              />
              <div style={{ textAlign: "center" }}>
                <p
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "#862D2D",
                  }}
                >
                  CREATE WALLET
                </p>
                <p
                  style={{
                    fontSize: 14,
                    color: "#862D2D",
                    opacity: 0.8,
                    marginTop: 15,
                    lineHeight: 2,
                    paddingRight: 40,
                    paddingLeft: 40,
                  }}
                >
                  Download TrustWallet or Metamask, the apps are secure and
                  widely use in the DeFi Market
                </p>
              </div>
              <div
                style={{
                  marginLeft: 220,
                  position: "absolute",
                  marginTop: -40,
                }}
              >
                <img src={pita1} alt="pita1" />
              </div>
            </div>
            <div
              style={{
                height: 330,
                width: 330,
                backgroundColor: "#EBEBEB",
                borderRadius: 10,
                borderWidth: 10,
                borderColor: "#4ABDB7",
              }}
            >
              <img
                src={coin}
                alt="coin"
                style={{ marginLeft: 90, marginTop: 20 }}
              />
              <div style={{ textAlign: "center" }}>
                <p
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "#862D2D",
                  }}
                >
                  FUND YOUR WALLET
                </p>
                <p
                  style={{
                    fontSize: 14,
                    color: "#862D2D",
                    opacity: 0.8,
                    marginTop: 15,
                    lineHeight: 2,
                    paddingRight: 40,
                    paddingLeft: 40,
                  }}
                >
                  Fund your wallet by purchasing BNB. Send BNB to your Metamask
                  Account
                </p>
              </div>
              <div
                style={{
                  marginLeft: 220,
                  position: "absolute",
                  marginTop: -40,
                }}
              >
                <img src={pita2} alt="pita2" />
              </div>
            </div>
            <div
              style={{
                height: 330,
                width: 330,
                backgroundColor: "#EBEBEB",
                borderRadius: 10,
                borderWidth: 10,
                borderColor: "#3CB878",
              }}
            >
              <img
                src={pancake}
                alt="pancake"
                style={{ marginLeft: 90, marginTop: 20 }}
              />
              <div style={{ textAlign: "center" }}>
                <p
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "#862D2D",
                  }}
                >
                  GO TO BROWSER
                </p>
                <p
                  style={{
                    fontSize: 14,
                    color: "#862D2D",
                    opacity: 0.8,
                    marginTop: 15,
                    lineHeight: 2,
                    paddingRight: 40,
                    paddingLeft: 40,
                  }}
                >
                  Use your Chrome and Visit Pancakeswap.finance
                </p>
              </div>
              <div
                style={{
                  marginLeft: 250,
                  position: "absolute",
                  marginTop: -10,
                }}
              >
                <img src={pita3} alt="pita3" />
              </div>
            </div>
          </div>
          <div style={{ marginLeft: 500, marginTop: 60 }} class="flex gap-20">
            <div
              style={{
                height: 330,
                width: 330,
                backgroundColor: "#EBEBEB",
                borderRadius: 10,
                borderWidth: 10,
                borderColor: "#C69C6D",
              }}
            >
              <img
                src={flokirush}
                alt="flokirush"
                style={{ marginLeft: 100, marginTop: 10 }}
              />
              <div style={{ textAlign: "center" }}>
                <p
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "#862D2D",
                  }}
                >
                  INSERT FLOKIRUSH CA
                </p>
                <p
                  style={{
                    fontSize: 14,
                    color: "#862D2D",
                    opacity: 0.8,
                    marginTop: 15,
                    lineHeight: 2,
                    paddingRight: 40,
                    paddingLeft: 40,
                  }}
                >
                  Click “Select a Currency” and enter FlokiRush contract address
                </p>
              </div>
              <div
                style={{
                  marginLeft: 220,
                  position: "absolute",
                  marginTop: -5,
                }}
              >
                <img src={pita4} alt="pita1" />
              </div>
            </div>
            <div
              style={{
                height: 330,
                width: 330,
                backgroundColor: "#EBEBEB",
                borderRadius: 10,
                borderWidth: 10,
                borderColor: "#F49AC1",
              }}
            >
              <img
                src={flush}
                alt="flush"
                style={{ marginLeft: 90, marginTop: 20 }}
              />
              <div style={{ textAlign: "center" }}>
                <p
                  style={{
                    fontWeight: "bold",
                    fontSize: 20,
                    color: "#862D2D",
                  }}
                >
                  PURCHASE $FLUSH
                </p>
                <p
                  style={{
                    fontSize: 14,
                    color: "#862D2D",
                    opacity: 0.8,
                    marginTop: 15,
                    lineHeight: 2,
                    paddingRight: 40,
                    paddingLeft: 40,
                  }}
                >
                  use the slippage 12% to 15% and purchase $FLUSH through
                  PancakeSwap
                </p>
              </div>
              <div
                style={{
                  marginLeft: 220,
                  position: "absolute",
                  marginTop: -40,
                }}
              >
                <img src={pita5} alt="pita5" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowToBuy;
