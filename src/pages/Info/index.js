import React from "react";
import bgSupport from "../../assets/image/bgsupport.svg";
import bgTartanBlue from "../../assets/image/bgTartanBlue.png";
import bgBrown from "../../assets/image/brownBanner.png";
import buttonBlue from "../../assets/image/buttonBlue.svg";
import buttonYellow from "../../assets/image/buttonYellow.svg";
import coinBank from "../../assets/image/coinBank.svg";
import logoFeatures from "../../assets/image/features.svg";
import flokiRun from "../../assets/image/flokiRun.png";
import miniGame from "../../assets/image/minigame.svg";
import nft from "../../assets/image/nft.svg";
import support from "../../assets/image/support.svg";
import top50 from "../../assets/image/top50.svg";

const Info = () => {
  return (
    <div
      style={{
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
      }}
    >
      <div
        style={{
          backgroundImage: `url(${bgTartanBlue}) `,
          height: 1600,
          width: "85%",
          marginTop: -20,
          justifyContent: "center",
          alignItems: "center",
          alignSelf: "center",
          zIndex: -1,
          marginLeft: 150,
        }}
      ></div>
      <div
        class="bg-no-repeat bottom-0 translate-y-96 translate-x-52"
        style={{
          backgroundImage: `url(${bgBrown}) `,
          height: 600,
          justifyContent: "center",
          alignItems: "center",
          alignSelf: "center",
          marginLeft: 200,
          marginTop: -150,
          zIndex: 2,
          position: "absolute",
          width: "70%",
        }}
      >
        <div
          style={{
            marginTop: 110,
            height: 200,
            width: 1000,
            marginLeft: 70,
            textAlign: "center",
            fontFamily: 'Arvo',
            color: "white",
          }}
        >
          <h1 style={{ fontSize: 50, fontWeight: "bold" }}>Meta FlokiRush</h1>
          <h1
            class="animate-fade-in-down"
            style={{ fontSize: 35, width: 800, marginLeft: 70, marginTop: 20, }}
          >
            Meta FlokiRush invites you to Rush along exciting journey to have
            tons of fun and make lots of money!!
          </h1>
        </div>
        <div
          style={{
            marginTop: 110,
            height: 200,
            width: 1000,
            marginLeft: 60,
            textAlign: "center",
            fontFamily: 'Arvo'
          }}
        >
          <h1
            class="animate-fade-in-down"
            style={{ fontSize: 35, fontWeight: "semiBold", opacity: 0.7  }}
          >
            Get Ready To Rush Your Way To Moon
          </h1>
        </div>
        <div style={{ marginLeft: -40 }}>
          <img src={support} alt="support" />
        </div>
        <div>
          <div class="flex flex-row justify-center gap-10 -ml-40 mt-10">
            <div class="flex flex-col">
              <img src={bgSupport} alt="bgSupport" style={{ marginTop: 30 }} />
              {/* <div class="flex flex-row">
                <img
                  src={imgTree}
                  alt="imgTree"
                  style={{ marginTop: -70, marginLeft: -60 }}
                />
                <img
                  src={gamingFloki}
                  alt="gamingFloki"
                  style={{ width: 250, marginLeft: -60 }}
                  // class="animate-bounce"
                />
              </div> */}
            </div>
            <div
              style={{
                width: 373,
                height: 400,
                backgroundColor: "whitesmoke",
                boxShadow: "9px 9px 16px -5px rgba(0,0,0,0.75)",
                zIndex: -2,
                borderRadius: 10,
                marginBottom: 50,
              }}
            >
              <div style={{ width: 400, marginLeft: -25 }}>
                <img src={flokiRun} alt="flokiRun" />
              </div>
            </div>
          </div>
        </div>
        <div
          style={{
            width: 1105,
            height: 642,
            borderRadius: 15,
            backgroundColor: "#EDE2D6",
            border: "dashed",
          }}
        >
          <div
            class="animate-bounce"
            style={{ marginLeft: 320, marginTop: 30 }}
          >
            <img src={logoFeatures} alt="logoFeatures" />
          </div>
          <div class="flex flex-row">
            <div
              style={{
                width: 250,
                height: 456,
                backgroundColor: "#EBEBEB",
                marginLeft: 20,
                marginTop: 30,
                borderRadius: 15,
                boxShadow: "10px 10px 10px 0px rgba(0,0,0,0.75)",
              }}
            >
              <div>
                <div>
                  <img
                    src={miniGame}
                    alt="miniGame"
                    class="rounded ml-2 pt-4 transition transform duration-500 hover:scale-125"
                    // style={{ borderRadius: 10, marginLeft: 8, paddingTop: 15, transition: 'transform 0.5' }}
                  />
                </div>
                <div
                  style={{
                    paddingLeft: 10,
                    paddingRight: 15,
                    textAlign: "justify",
                    wordBreak: "break-all",
                    fontFamily: 'Arvo'
                  }}
                >
                  <p
                    style={{
                      color: "#8B3E39",
                      fontWeight: "bold",
                      marginBottom: 5,
                    }}
                  >
                    FlokiRush Game
                  </p>
                  <p style={{ opacity: 0.8, fontSizeAdjust: 2 }}>
                    Experience fun and simple minigame concept and unlimited
                    possibilities for lifetime earnings.
                  </p>
                </div>
                <div class="transition transform duration-500 hover:scale-95">
                  <img
                    onClick={() => {}}
                    src={buttonYellow}
                    alt="buttonYellow"
                    style={{ marginLeft: -20, marginTop: 25 }}
                  />
                </div>
              </div>
            </div>
            <div
              // class=' transition transform duration-500 hover:scale-125'
              style={{
                width: 250,
                height: 456,
                backgroundColor: "#EBEBEB",
                marginLeft: 20,
                marginTop: 30,
                borderRadius: 15,
                boxShadow: "10px 10px 10px 0px rgba(0,0,0,0.75)",
              }}
            >
              <div>
                <div>
                  <img
                    src={coinBank}
                    alt="coinBank"
                    // style={{ borderRadius: 10, marginLeft: 8, paddingTop: 15 }}
                    class="rounded ml-2 pt-4 transition transform duration-500 hover:scale-125"
                  />
                </div>
                <div
                  style={{
                    paddingLeft: 10,
                    paddingRight: 15,
                    textAlign: "justify",
                    wordBreak: "break-all",
                    fontFamily: 'Arvo'
                  }}
                >
                  <p
                    style={{
                      color: "#8B3E39",
                      fontWeight: "bold",
                      marginBottom: 5,
                    }}
                  >
                    Doge Coinbank
                  </p>
                  <p style={{ opacity: 0.8 }}>
                    Allocation goes into the ADA Vault,about 10% from total
                    transaction and 10% between wallets
                  </p>
                </div>
                <div class="transition transform duration-500 hover:scale-95">
                  <img
                    onClick={() => {}}
                    src={buttonBlue}
                    alt="buttonBlue"
                    style={{ marginLeft: -20, marginTop: 20 }}
                  />
                </div>
              </div>
            </div>
            <div
              style={{
                width: 250,
                height: 456,
                backgroundColor: "#EBEBEB",
                marginLeft: 20,
                marginTop: 30,
                borderRadius: 15,
                boxShadow: "10px 10px 10px 0px rgba(0,0,0,0.75)",
              }}
            >
              <div>
                <div>
                  <img
                    src={top50}
                    alt="top50"
                    // style={{ borderRadius: 10, marginLeft: 8, paddingTop: 15 }}
                    class="rounded ml-2 pt-4 transition transform duration-500 hover:scale-125"
                  />
                </div>
                <div
                  style={{
                    paddingLeft: 10,
                    paddingRight: 15,
                    textAlign: "justify",
                    wordBreak: "break-all",
                    fontFamily: 'Arvo'
                  }}
                >
                  <p
                    style={{
                      color: "#8B3E39",
                      fontWeight: "bold",
                      marginBottom: 5,
                    }}
                  >
                    Top 50 Club Incentive
                  </p>
                  <p style={{ opacity: 0.8 }}>
                    Top 50 Club Incentive Hodl and DCA your way to be a Top 50
                    club and claim daily rewards incentivized specifically for
                    them.
                  </p>
                </div>
                <div class="transition transform duration-500 hover:scale-95">
                  <img
                    onClick={() => {}}
                    src={buttonYellow}
                    alt="buttonYellow"
                    style={{ marginLeft: -20 }}
                  />
                </div>
              </div>
            </div>
            <div
              style={{
                width: 250,
                height: 456,
                backgroundColor: "#EBEBEB",
                marginLeft: 20,
                marginTop: 30,
                borderRadius: 15,
                boxShadow: "10px 10px 10px 0px rgba(0,0,0,0.75)",
              }}
            >
              <div>
                <div>
                  <img
                    src={nft}
                    alt="nft"
                    // style={{ borderRadius: 10, marginLeft: 8, paddingTop: 15 }}
                    class="rounded ml-2 pt-4 transition transform duration-500 hover:scale-125"
                  />
                </div>
                <div
                  style={{
                    paddingLeft: 10,
                    paddingRight: 15,
                    textAlign: "justify",
                    wordBreak: "break-all",
                    fontFamily: 'Arvo'
                  }}
                >
                  <p
                    style={{
                      color: "#8B3E39",
                      fontWeight: "bold",
                      marginBottom: 5,
                    }}
                  >
                    Upcoming NFTs
                  </p>
                  <p style={{ opacity: 0.8 }}>
                    Experience fun and simple minigame concept and unlimited
                    possibilities for lifetime earnings.
                  </p>
                </div>
                <div class="transition transform duration-500 hover:scale-95">
                  <img
                    onClick={() => {}}
                    src={buttonBlue}
                    alt="buttonBlue"
                    style={{ marginLeft: -20, marginTop: 20 }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Info;
