import React from "react";
import logo from "../../assets/image/logoFlokiRush.png";
import Navbar from "../../components/molecules/Navbar";
import bgVideo from "../../assets/video/flokiRush.mp4";
import Logo from "../../components/atoms/Logo";

const twitter = "./icon/twitter.svg";
const facebook = "./icon/facebook.svg";
const telegram = "./icon/telegram.svg";
const instagram = "./icon/instagram.png";

function Header() {
  return (
    <>
      <div>
        <div id="header">
          <video autoPlay loop muted class=" w-full h-4/5 object-cover -z-30">
            <source src={bgVideo} type="video/mp4" />
          </video>
          <div
            class="bg-[url('../public/img/greenPolkadot.svg')] flex absolute w-full bg-cover py-4 gap-3 
        px-10 justify-center laptop:justify-start top-0  "
          >
            <div class="flex justify-between w-full items-center">
              <div class="flex">
                <a href="#">
                  <img
                    src={twitter}
                    class="transition transform duration-500 hover:scale-125 hover:animate-spin"
                    style={{ width: 45, height: 45 }}
                  />
                </a>
                <a href="#">
                  <img
                    src={facebook}
                    class="transition transform duration-500 hover:scale-125 hover:animate-spin"
                    style={{ width: 45, height: 45 }}
                  />
                </a>
                <a href="#">
                  <img
                    src={telegram}
                    class="transition transform duration-500 hover:scale-125 hover:animate-spin"
                    style={{ width: 50, height: 50 }}
                  />
                </a>
                <a href="#">
                  <img
                    src={instagram}
                    class="transition transform duration-500 hover:scale-125 hover:animate-spin"
                    style={{ width: 40, height: 40 }}
                  />
                </a>
              </div>
              <div
                class="transition transform duration-500 hover:scale-125 py-2 px-3 rounded text-red-700, font-medium "
                style={{ backgroundColor: "#FFC84E" }}
              >
                <p style={{ fontFamily: "Arvo", fontWeight: "600", color:'#922626' }}>
                  Dashboard
                </p>
              </div>
            </div>
            <Navbar />
          </div>
          <Logo />
        </div>
      </div>
    </>
  );
}

export default Header;
