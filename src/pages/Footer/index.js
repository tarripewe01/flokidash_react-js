import React from "react";
import bgFooter from "../../assets/image/bgFooter2.svg";
const twitter = "./icon/twitter.svg";
const facebook = "./icon/facebook.svg";
const telegram = "./icon/telegram.svg";
const instagram = "./icon/instagram.png";

const Footer = () => {
  return (
    <div>
      <div
        style={{
          backgroundImage: `url(${bgFooter}) `,
          backgroundRepeat: "no-repeat",
          height: 920,
          //   width: 3000,
          marginTop: -300,
          marginLeft: -5,
        }}
      >
        <div class="flex flex-col  justify-center items-center translate-y-48">
          <div
            class="flex justify-start gap-40"
            style={{ marginTop: 170, fontFamily: "Arvo" }}
          >
            <div style={{ textAlign: "start", color: "#413838" }}>
              <p style={{ fontSize: 36, fontWeight: "bold", marginBottom: 10 }}>
                CURIOUS?
                <br /> GET IN TOUCH
              </p>
              <p style={{ fontSize: 20 }}>
                We can’t promise a reply, but we’d love
                <br /> to hear your thoughts
              </p>
            </div>
            <div style={{ textAlign: "start", color: "#413838" }}>
              <p style={{ fontSize: 36, fontWeight: "bold", marginBottom: 10 }}>
                INFO
              </p>
              <p style={{ fontSize: 20 }}>Term and Conditions</p>
            </div>
            <div style={{ textAlign: "start", color: "#413838" }}>
              <p style={{ fontSize: 36, fontWeight: "bold", marginBottom: 10 }}>
                FOLLOW US
              </p>
              <div class="flex gap-1">
                <img
                  src={telegram}
                  alt=" twitter"
                  class="transition transform duration-500 hover:scale-125 hover:animate-spin"
                  style={{ width: 50, height: 50 }}
                />
                <img
                  src={twitter}
                  alt=" twitter"
                  class="transition transform duration-500 hover:scale-125 hover:animate-spin"
                  style={{ width: 45, height: 45 }}
                />
                <img
                  src={facebook}
                  alt=" twitter"
                  class="transition transform duration-500 hover:scale-125 hover:animate-spin"
                  style={{ width: 45, height: 45 }}
                />
                <img
                  src={instagram}
                  alt=" twitter"
                  class="transition transform duration-500 hover:scale-125 hover:animate-spin"
                  style={{ width: 45, height: 45 }}
                />
              </div>
            </div>
            <div style={{ textAlign: "start", color: "#413838" }}>
              <p style={{ fontSize: 36, fontWeight: "bold", marginBottom: 10 }}>
                CONTACT US
              </p>
              <p style={{ fontSize: 20 }}>metaflokirush@gmail.com</p>
            </div>
          </div>
          <div class="mt-80">
            <p style={{ color: "#fff", fontSize: 25 }}>
              Copyright © 2022 MetaFlokiRush. All Right Reserved.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
