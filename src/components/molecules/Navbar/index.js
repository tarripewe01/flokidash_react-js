import React from "react";

const Navbar = () => {
  return (
    <div class="bg-[url('../public/img/yellowRibbon.svg')] hidden laptop:flex w-4/5 translate-x-1/3 bg-no-repeat absolute p-20">
      <ul class=" flex gap-3 absolute lg:w-2/6 top-10 items-center justify-center laptop:ml-3 py-3   ">
        <a
          href="#"
          class="font-['Aclonica'] text-red-700 hover:text-red-900 uppercase transition transform duration-500 hover:scale-125"
        >
          <li>Home</li>
        </a>
        <a
          href="#"
          class="font-['Aclonica'] text-red-700 hover:text-red-900 uppercase transition transform duration-500 hover:scale-125 "
        >
          <li>Features</li>
        </a>
        <a
          href="#"
          class="font-['Aclonica'] text-red-700 hover:text-red-900 uppercase  transition transform duration-500 hover:scale-125"
        >
          <li>Roadmap</li>
        </a>
        <a
          href="#"
          class="font-['Aclonica'] text-red-700 hover:text-red-900 uppercase transition transform duration-500 hover:scale-125 "
        >
          <li>Whitepaper</li>
        </a>
        <a
          href="#"
          class="font-['Aclonica'] text-red-700 hover:text-red-900 uppercase transition transform duration-500 hover:scale-125 "
        >
          <li>minigame</li>
        </a>
        <a
          href="#"
          class="font-['Aclonica'] text-red-700 hover:text-red-900 uppercase transition transform duration-500 hover:scale-125 "
        >
          <li>Marketplace</li>
        </a>
      </ul>
    </div>
  );
};

export default Navbar;
