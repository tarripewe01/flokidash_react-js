import React from "react";
import logo from "../../../assets/image/logoMetalFloki.png";

const Logo = () => {
  return (
    <div class=" flex absolute top-32 w-full translate-x-1/4 ml-20 laptop:ml-32">
      <img src={logo} class="w-2/5 " />
    </div>
  );
};

export default Logo;
